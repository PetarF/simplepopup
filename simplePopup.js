/* 
 * SimplePopup
 * 
 * @requires jQuery >1.2
 * @author Petar Fedorovsky @ Dimedia http://dimedia.hr
 * plugin for simple creation of popup's
 * 
 * Licensed under the MIT licence:
 * http://www.opensource.org/licenses/mit-license.php
 */
var simplePopupContainer = [];
var SimplePopup = function (options) {

    this.settings = {
        'DEBUG':false,
        'maxOverlays':10,
        'cascade':false, /* if true display popups relative to previously opened popup */
        'relativeElement':false,/* Jquery selector - display popups relative to element */
        'content':false,
       /* { //object,false
            url:false, // valid url for post action that returns HTML content, false
            data:false,// JSON object for parameters of post action, false, inline html that will be inserted
        }, */
        'overlayClass':'simplePopupOverlay_',
        'contentClass':'simplePopupContent_',
        /*HIDDEN CONTENT*/
        'contentHtmlId':'simplePopupContentHtml',
        'contentHtmlReplace':false, //on destroy content of contentHtmlId will be replaced by content from popup
        
        'closeBtn':'.simplePopupClose',
        'overlayCss':{
            'display': 'table', /* Content height fix */
            'position': 'absolute', /* Stay in place */
            'z-index': '3000', /* Sit on top */
            'left': '0',
            'top': '0',
            'width': '100%', /* Full width */
            'height': '100%', /* Full height */
            'overflow': 'auto', /* Enable scroll if needed */
            'background-color': 'rgb(0,0,0)', /* Fallback color */
            'background-color': 'rgba(0,0,0,0.8)' /* Black w/ opacity */
            
        },
        'contentCss':{
            'background-color': '#fefefe',
            'margin': 'auto',
            'padding': '20px',
            'border': '1px solid #888',
            'width': '60%',
            'position':'relative',
            'top':'50px',
            'height':'auto',
            'display':'table'
           
        },
        'contentClassInsert':false,
        'overlayClassInsert':false,
        'optionalData':false,
        onBeforeOverlay: function (el,SimplePopup) {},
        onAfterOverlayShow: function (el,SimplePopup) {},
        onBeforeContent: function (el,SimplePopup) {},
        onAfterContentShow: function (el,SimplePopup) {},
        onBeforeContentClose: function (el,SimplePopup) {},
        showContentMethod: function (el,SimplePopup) {
            $(el).show();
        },
        closeContentMethod: function (el,SimplePopup) {
            $(el).fadeOut("slow", function() {
                SimplePopup.removeFromDOM(SimplePopup);
            });
        },
        'ajax':{
                        type: "POST",
                        dataType: 'html',
                       // async: false                       
            },
        preloaderStyles:'#simplePopupPreloaderAfter,#simplePopupPreloaderBefore{content:"";position:absolute}#simplePopupPreloader{display:block;position:absolute;left:50%;top:50%;width:150px;height:150px;margin:-75px 0 0 -75px;border-radius:50%;border:3px solid transparent;border-top-color:#F00;-webkit-animation:spin 1.5s linear infinite;animation:spin 1.5s linear infinite;z-index:1001}#simplePopupPreloaderBefore{top:5px;left:5px;right:5px;bottom:5px;border-radius:50%;border:3px solid transparent;border-top-color:#F00;-webkit-animation:spin 2.5s linear infinite;animation:spin 2.5s linear infinite}#simplePopupPreloaderAfter{top:15px;left:15px;right:15px;bottom:15px;border-radius:50%;border:3px solid transparent;border-top-color:#F00;-webkit-animation:spin 1.5s linear infinite;animation:spin 1.5s linear infinite}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0);-ms-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{0%{-webkit-transform:rotate(0);-ms-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}',
        preloaderColor:false
    };

    this.overlayClassName = '';
    this.overlayClass = '';
    this.contentClassName = '';
    this.contentClass = '';
    this.containerId = '';
    this.html = '';
    
    this.validateUrl = function (url) {
        return /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/i.test(url);
    }
    
    this.getContent = function (self) {

        var html = $('#' + self.settings.contentHtmlId);
        //var ret = false;
        if (html.length) {
            if (self.settings.DEBUG) {
                console.log('Id with HTML data: ' + html.html());
            }
            self.html = html.html();
            self.showContent(self);
        }
        if (self.settings.content != false) {
            if (self.settings.DEBUG) {
                        console.log('content not empty');
                    }
            if (self.settings.content.url != false) {
                if (self.settings.DEBUG) {
                        console.log('url not empty :'+self.settings.content.url);
                        console.log(self.validateUrl(self.settings.content.url));
                    }
                if (self.validateUrl(self.settings.content.url)) {
                    if (self.settings.DEBUG) {
                        console.log('URL FOUND, POST :' + self.settings.content.url);
                    }
                    var postData = (typeof self.settings.content.data == 'object') ? self.settings.content.data : {};
                    
                    var ajaxDefaultSettings = self.settings.ajax;
                    var ajaxSettings = {
                        url: self.settings.content.url,
                        data: postData,
                        success: function (data) {
                            //ret = data;
                            self.html = data;
                            self.showContent(self);
                        }
                    }
                    
                    if(typeof self.settings.ajax.success === 'function'){
                        ajaxSettings.success = self.settings.ajax.success;
                    }
                    
                    /* Merge settings. */
                    $.extend(true,ajaxDefaultSettings, ajaxSettings);
                    
                    $.ajax(ajaxDefaultSettings);

                }

            } else if (self.settings.content.data !=false && typeof self.settings.content.data != 'object') {
                if (self.settings.DEBUG) {
                    console.log('Inline HTML data: ' + self.settings.content.data);
                }
                //ret = self.settings.content.data;
                self.html = self.settings.content.data;
                self.showContent(self);
            }
        }else{
            self.html = '';
            self.showContent(self);
        }

    }
    
    this.getClassName = function(className){
        var self = this;
        var returnClassName = className+'1';
        
        for (i = 0; i < self.settings.maxOverlays; i++) {
            if($('.'+className+i).length == 0){
                returnClassName = className+i;
                break;
            }
        }
        
        return returnClassName;
    }
    
    this.removeFromDOM = function (self) {
        
            $(self.overlayClass).remove();
            if (self.settings.DEBUG) {
                console.log('removefd', self);
                console.log('overlay removed');
            }
            $(self.overlayClass).unbind('click');
            $(self.contentClass).unbind('click');
            $(self.settings.closeBtn).unbind('click');
            simplePopupContainer.splice(self.containerId, 1);
    }
    
    this.removePopup = function (self) {
        self.settings.onBeforeContentClose($(self.contentClass), self);
        if(self.settings.contentHtmlReplace){
            var currentHtml = $(self.contentClass).html();
            $('#' + self.settings.contentHtmlId).html(currentHtml);
        }
        
        self.settings.closeContentMethod($(self.overlayClass), self);
    }  
    
    this.calculateTopPosition = function (self) {
        var userTop = (self.settings.contentCss.top) ? self.settings.contentCss.top.match(/\d+(?:\.\d+)?/)[0] : 0;
        var actualTop = (parseInt(window.scrollY) + parseInt(userTop));
        var userUnit = (self.settings.contentCss.top) ? self.settings.contentCss.top.split(userTop)[1] : 'px';

        if (self.settings.cascade != false) {
            if (self.settings.DEBUG) {
                console.log('cascade is on');
            }
            
            var matches = self.contentClassName.match(/\d+/);
            var num = parseInt(matches[0]);
            
            if (self.settings.DEBUG) {
                console.log('popup sufix: '+num);
            }
            
            if (num >= 1) {
                var el = '.' + self.contentClassName.replace(num, (num - 1));
                if($(el).length){
                   actualTop = (parseInt($(el).position()['top']) + parseInt(userTop)); 
                }
            }
        }

        if (self.settings.relativeElement != false) {
            actualTop = (parseInt($(self.settings.relativeElement).offset()['top']) + parseInt(userTop));
        }

        return actualTop;
    }
    
    this.createContainer = function (self) {
            if (self.settings.DEBUG) {
                console.log('before create container');
            }
        self.settings.onBeforeContent($(self.contentClass), self);
        $(self.overlayClass).append('<div class="' + self.contentClassName +'"></div>');
        if (self.settings.DEBUG) {
                console.log('calculate position');
            }
        
        self.settings.contentCss['top'] = self.calculateTopPosition(self);
        $(self.contentClass).css(self.settings.contentCss).addClass(self.settings.contentClassInsert);

        $(self.contentClass).on('click',function (e) {
            e.stopPropagation();            
        });
        
        $(self.contentClass).on('click', self.settings.closeBtn,function (e) {
            e.stopPropagation();
            self.removePopup(self);
        });

        var dh = $(document).height();
        $(self.overlayClass).height( dh );
    }    
      
    this.create = function () {
        var self = this;
        self.overlayClassName = self.getClassName(self.settings.overlayClass);
        self.overlayClass = '.' + self.overlayClassName;
        self.contentClassName = self.getClassName(self.settings.contentClass);
        self.contentClass = '.' + self.contentClassName;

        /* Overlay creation */
        self.settings.onBeforeOverlay($(self.overlayClass), self);
        if (self.settings.DEBUG) {
                console.log('overlay....');
                console.log('<div class="' + self.overlayClassName +'"></div>');
            }
        $('body').append('<div class="' + self.overlayClassName +'"></div>');
        $(self.overlayClass).css(self.settings.overlayCss).addClass(self.settings.overlayClassInsert);
        
        $(self.overlayClass).on('click', function () {
            self.removePopup(self);
        });
        
        $(self.overlayClass).show();
        self.settings.onAfterOverlayShow($(self.overlayClass), self);
        /* End overlay creation */

        /* Container creation */
        self.createContainer(self);
        /* End container creation */
        
        if (self.settings.DEBUG) {
                console.log('before get contents');
            }
        self.getContent(self);
    };
    
    this.showContentRunning = false;
    
    this.showContent = function(self){
        
        if(self.showContentRunning){
            return;
        }
        self.showContentRunning = true;
        
        if (self.settings.DEBUG) {
                console.log('after get contents');
            }
        
        if (self.html) {            
            $(self.contentClass).append(self.html);
            if (self.settings.DEBUG) {
                console.log('html object found');
            }
        } else {
            if (self.settings.DEBUG) {
                console.log('html object not found');
            }
        }
        
        self.settings.showContentMethod($(self.contentClass), self);
        self.settings.onAfterContentShow($(self.contentClass), self);
    }
    
    this.preloaderStart = function(){
        el = $('.'+this.contentClassName);
        el.append('<div id="simplePopupPreloader"><style>'+this.settings.preloaderStyles+'</style><div id="simplePopupPreloaderAfter"></div><div id="simplePopupPreloaderBefore"></div></div>');
        if(this.settings.preloaderColor != false){
           el.find('#simplePopupPreloader,#simplePopupPreloaderAfter,#simplePopupPreloaderBefore').css('border-top-color',this.settings.preloaderColor); 
        }
        
    }
    this.preloaderEnd = function(){
        el = $('.'+this.contentClassName+' #simplePopupPreloader').remove();
    }
    /* Constructor */
    {
        /* Merge settings. */
        $.extend(true,this.settings, options);
        /* Make the request. */
        this.create();
        
        simplePopupContainer.push(this);
        this.containerId = simplePopupContainer.length -1;
    }
};

